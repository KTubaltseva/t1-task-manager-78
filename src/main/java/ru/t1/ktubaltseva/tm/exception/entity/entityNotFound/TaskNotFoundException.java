package ru.t1.ktubaltseva.tm.exception.entity.entityNotFound;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
