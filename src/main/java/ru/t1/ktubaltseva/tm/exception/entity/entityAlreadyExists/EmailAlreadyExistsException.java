package ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists;

import org.jetbrains.annotations.NotNull;

public final class EmailAlreadyExistsException extends AbstractAlreadyExistsException {

    public EmailAlreadyExistsException() {
        super("Error! Email already exists...");
    }

    public EmailAlreadyExistsException(@NotNull final String message) {
        super("Error! Email \"" + message + "\" already exists...");
    }
}
