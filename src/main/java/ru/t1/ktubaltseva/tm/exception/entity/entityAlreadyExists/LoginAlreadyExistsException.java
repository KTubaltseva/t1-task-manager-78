package ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists;

import org.jetbrains.annotations.NotNull;

public final class LoginAlreadyExistsException extends AbstractAlreadyExistsException {

    public LoginAlreadyExistsException() {
        super("Error! Login already exists...");
    }

    public LoginAlreadyExistsException(@NotNull final String message) {
        super("Error! Login \"" + message + "\" already exists...");
    }
}
