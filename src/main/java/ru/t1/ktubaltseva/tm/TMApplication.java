package ru.t1.ktubaltseva.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TMApplication {

    public static void main(String[] args) {
        SpringApplication.run(TMApplication.class, args);
    }

}
