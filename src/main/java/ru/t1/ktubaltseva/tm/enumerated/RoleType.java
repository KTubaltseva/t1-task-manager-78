package ru.t1.ktubaltseva.tm.enumerated;

import lombok.Getter;

@Getter
public enum RoleType {

    USUAL,
    ADMIN

}
