package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
public final class Task extends AbstractUserOwnedModelWBS {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id", columnDefinition = "varchar(36)")
    private Project project;

    public Task(@Nullable String name) {
        super(name);
    }


    public Task() {
        super();
    }

}
