package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_role")
public final class Role {

    private static final long serialVersionUID = 0;

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "varchar(36)", nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role_type", columnDefinition = "varchar(30)", nullable = false)
    private RoleType roleType = RoleType.USUAL;

    @NotNull
    @ManyToOne
    @JsonIgnore
    private User user;

    @Override
    public String toString() {
        return roleType.toString();
    }
}
