package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
public class AbstractModel implements Serializable {

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "varchar(36)", nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @Column(name = "created", columnDefinition = "timestamp default current_date", nullable = false)
    private Date created = new Date();

}
