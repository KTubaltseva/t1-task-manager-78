package ru.t1.ktubaltseva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.rest.ITaskRestEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService service;

    @NotNull
    @Override
    @PutMapping("/create")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public TaskDTO create() throws AbstractException {
        return service.create(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PutMapping("/add")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public TaskDTO add(@RequestBody @NotNull final TaskDTO task) throws AbstractException {
        return service.add(UserUtil.getUserId(), task);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        service.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.existsById(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public TaskDTO findById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.findById(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @PostMapping("/update/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public TaskDTO update(@RequestBody @NotNull final TaskDTO task) throws AbstractException {
        return service.update(UserUtil.getUserId(), task);
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public List<TaskDTO> findAll() throws AbstractException {
        return service.findAll(UserUtil.getUserId());
    }


    @Override
    @DeleteMapping("/clear")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public void clear() throws AbstractException {
        service.clear(UserUtil.getUserId());
    }

    @Override
    @GetMapping("/count")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public long count() throws AbstractException {
        return service.count(UserUtil.getUserId());
    }

}

