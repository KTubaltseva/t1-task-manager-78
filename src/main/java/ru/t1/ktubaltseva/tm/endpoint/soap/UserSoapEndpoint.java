package ru.t1.ktubaltseva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.api.endpoint.soap.IUserSoapEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.dto.soap.user.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;

@Endpoint
public class UserSoapEndpoint implements IUserSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "UserSoapEndpointPort";

    public final static String NAMESPACE = "http://ktubaltseva.t1.ru/tm/dto/soap/user";

    @NotNull
    @Autowired
    private IUserDTOService service;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userCountRequest", namespace = NAMESPACE)
    public UserCountResponse count(
            @RequestPayload @NotNull final UserCountRequest request
    ) throws AbstractException {
        return new UserCountResponse(service.count());
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userClearRequest", namespace = NAMESPACE)
    public UserClearResponse clear(
            @RequestPayload @NotNull final UserClearRequest request
    ) throws AbstractException {
        service.clear();
        return new UserClearResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userDeleteByIdRequest", namespace = NAMESPACE)
    public UserDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final UserDeleteByIdRequest request
    ) throws AbstractException {
        service.deleteById(request.getId());
        return new UserDeleteByIdResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userExistsByIdRequest", namespace = NAMESPACE)
    public UserExistsByIdResponse existsById(
            @RequestPayload @NotNull final UserExistsByIdRequest request
    ) throws AbstractException {
        return new UserExistsByIdResponse(service.existsById(request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userFindAllRequest", namespace = NAMESPACE)
    public UserFindAllResponse findAll(
            @RequestPayload @NotNull final UserFindAllRequest request
    ) throws AbstractException {
        return new UserFindAllResponse(service.findAll());
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userFindByIdRequest", namespace = NAMESPACE)
    public UserFindByIdResponse findById(
            @RequestPayload @NotNull final UserFindByIdRequest request
    ) throws AbstractException {
        return new UserFindByIdResponse(service.findById(request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userFindByLoginRequest", namespace = NAMESPACE)
    public UserFindByLoginResponse findByLogin(
            @RequestPayload @NotNull final UserFindByLoginRequest request
    ) throws AbstractException {
        return new UserFindByLoginResponse(service.findByLogin(request.getLogin()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userSaveRequest", namespace = NAMESPACE)
    public UserSaveResponse save(
            @RequestPayload @NotNull final UserSaveRequest request
    ) throws EntityNotFoundException {
        @NotNull final UserDTO user = request.getUser();
        return new UserSaveResponse(service.add(user));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "userUpdateRequest", namespace = NAMESPACE)
    public UserUpdateResponse update(
            @RequestPayload @NotNull final UserUpdateRequest request
    ) throws AbstractException {
        @NotNull final UserDTO user = request.getUser();
        return new UserUpdateResponse(service.update(user));
    }

}

