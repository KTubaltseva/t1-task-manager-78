package ru.t1.ktubaltseva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.api.endpoint.soap.IAuthSoapEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.soap.auth.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import javax.annotation.Resource;

@Endpoint
public class AuthSoapEndpoint implements IAuthSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "AuthSoapEndpointPort";

    public final static String NAMESPACE = "http://ktubaltseva.t1.ru/tm/dto/soap/auth";

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "loginRequest", namespace = AuthSoapEndpoint.NAMESPACE)
    public AuthLoginResponse login(
            @RequestPayload @NotNull final AuthLoginRequest request
    ) {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new AuthLoginResponse(authentication.isAuthenticated());
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "logoutRequest", namespace = AuthSoapEndpoint.NAMESPACE)
    public AuthLogoutResponse logout(
            @RequestPayload @NotNull final AuthLogoutRequest request
    ) {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new AuthLogoutResponse(true);
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "profileRequest", namespace = AuthSoapEndpoint.NAMESPACE)
    public AuthProfileResponse profile(
            @RequestPayload @NotNull final AuthProfileRequest request
    ) throws AbstractException {
        @NotNull final SecurityContext context = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = context.getAuthentication();
        @NotNull final String login = authentication.getName();
        return new AuthProfileResponse(userDTOService.findByLogin(login));
    }

}
