package ru.t1.ktubaltseva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.api.endpoint.soap.ITaskSoapEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.soap.task.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

@Endpoint
public class TaskSoapEndpoint implements ITaskSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSOAPEndpointPort";

    public final static String NAMESPACE = "http://ktubaltseva.t1.ru/tm/dto/soap/task";

    @Autowired
    private ITaskDTOService service;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    public TaskCreateResponse create(@RequestPayload @NotNull final TaskCreateRequest request) throws AbstractException {
        @NotNull final TaskCreateResponse response = new TaskCreateResponse();
        @NotNull final TaskDTO task = service.create(UserUtil.getUserId());
        response.setTask(task);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskAddRequest", namespace = NAMESPACE)
    public TaskAddResponse add(@RequestPayload @NotNull final TaskAddRequest request) throws AbstractException {
        @NotNull final TaskAddResponse response = new TaskAddResponse();
        @NotNull final TaskDTO requestTask = request.getTask();
        @NotNull final TaskDTO responseTask = service.add(UserUtil.getUserId(), requestTask);
        response.setTask(responseTask);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload @NotNull final TaskDeleteByIdRequest request) throws AbstractException {
        @NotNull final TaskDeleteByIdResponse response = new TaskDeleteByIdResponse();
        @NotNull final String requestId = request.getId();
        service.deleteById(UserUtil.getUserId(), requestId);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(@RequestPayload @NotNull final TaskExistsByIdRequest request) throws AbstractException {
        @NotNull final TaskExistsByIdResponse response = new TaskExistsByIdResponse();
        @NotNull final String requestId = request.getId();
        final boolean responseExists = service.existsById(UserUtil.getUserId(), requestId);
        response.setExists(responseExists);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload @NotNull final TaskFindByIdRequest request) throws AbstractException {
        @NotNull final TaskFindByIdResponse response = new TaskFindByIdResponse();
        @NotNull final String requestId = request.getId();
        @NotNull final TaskDTO responseTask = service.findById(UserUtil.getUserId(), requestId);
        response.setTask(responseTask);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    public TaskUpdateResponse update(@RequestPayload @NotNull final TaskUpdateRequest request) throws AbstractException {
        @NotNull final TaskUpdateResponse response = new TaskUpdateResponse();
        @NotNull final TaskDTO requestTask = request.getTask();
        @NotNull final TaskDTO responseTask = service.update(UserUtil.getUserId(), requestTask);
        response.setTask(responseTask);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload @NotNull final TaskFindAllRequest request) throws AbstractException {
        @NotNull final TaskFindAllResponse response = new TaskFindAllResponse();
        @NotNull final List<TaskDTO> responseTasks = service.findAll(UserUtil.getUserId());
        response.setTasks(responseTasks);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = NAMESPACE)
    public TaskFindAllByProjectIdResponse findAllByProjectId(@RequestPayload @NotNull final TaskFindAllByProjectIdRequest request) throws AbstractException {
        @NotNull final TaskFindAllByProjectIdResponse response = new TaskFindAllByProjectIdResponse();
        @NotNull final List<TaskDTO> responseTasks = service.findAllByUserIdAndProjectId(UserUtil.getUserId(), request.getProjectId());
        response.setTask(responseTasks);
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload @NotNull final TaskClearRequest request) throws AbstractException {
        @NotNull final TaskClearResponse response = new TaskClearResponse();
        service.clear(UserUtil.getUserId());
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@RequestPayload @NotNull final TaskCountRequest request) throws AbstractException {
        @NotNull final TaskCountResponse response = new TaskCountResponse();
        final long responseCount = service.count(UserUtil.getUserId());
        response.setCount(responseCount);
        return response;
    }

}

