package ru.t1.ktubaltseva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.rest.IProjectRestEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Override
    @PutMapping("/create")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public ProjectDTO create() throws AbstractException {
        return service.create(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PutMapping("/add")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public ProjectDTO add(@RequestBody @NotNull final ProjectDTO project) throws AbstractException {
        return service.add(UserUtil.getUserId(), project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        service.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.existsById(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public ProjectDTO findById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.findById(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @PostMapping("/update/{id}")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public ProjectDTO update(@RequestBody @NotNull final ProjectDTO project) throws AbstractException {
        return service.update(UserUtil.getUserId(), project);
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public List<ProjectDTO> findAll() throws AbstractException {
        return service.findAll(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/clear")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public void clear() throws AbstractException {
        service.clear(UserUtil.getUserId());
    }

    @Override
    @GetMapping("/count")
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public long count() throws AbstractException {
        return service.count(UserUtil.getUserId());
    }

}

