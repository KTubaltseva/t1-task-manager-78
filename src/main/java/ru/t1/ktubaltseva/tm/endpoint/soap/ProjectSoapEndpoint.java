package ru.t1.ktubaltseva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.api.endpoint.soap.IProjectSoapEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.soap.project.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

@Endpoint
public class ProjectSoapEndpoint implements IProjectSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSOAPEndpointPort";

    public final static String NAMESPACE = "http://ktubaltseva.t1.ru/tm/dto/soap/project";

    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(@RequestPayload @NotNull final ProjectCreateRequest request) throws AbstractException {
        @NotNull final ProjectCreateResponse response = new ProjectCreateResponse();
        @NotNull final ProjectDTO project = service.create(UserUtil.getUserId());
        response.setProject(project);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectAddRequest", namespace = NAMESPACE)
    public ProjectAddResponse add(@RequestPayload @NotNull final ProjectAddRequest request) throws AbstractException {
        @NotNull final ProjectAddResponse response = new ProjectAddResponse();
        @NotNull final ProjectDTO requestProject = request.getProject();
        @NotNull final ProjectDTO responseProject = service.add(UserUtil.getUserId(), requestProject);
        response.setProject(responseProject);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload @NotNull final ProjectDeleteByIdRequest request) throws AbstractException {
        @NotNull final ProjectDeleteByIdResponse response = new ProjectDeleteByIdResponse();
        @NotNull final String requestId = request.getId();
        service.deleteById(UserUtil.getUserId(), requestId);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@RequestPayload @NotNull final ProjectExistsByIdRequest request) throws AbstractException {
        @NotNull final ProjectExistsByIdResponse response = new ProjectExistsByIdResponse();
        @NotNull final String requestId = request.getId();
        final boolean responseExists = service.existsById(UserUtil.getUserId(), requestId);
        response.setExists(responseExists);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload @NotNull final ProjectFindByIdRequest request) throws AbstractException {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        @NotNull final String requestId = request.getId();
        @NotNull final ProjectDTO responseProject = service.findById(UserUtil.getUserId(), requestId);
        response.setProject(responseProject);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = NAMESPACE)
    public ProjectUpdateResponse update(@RequestPayload @NotNull final ProjectUpdateRequest request) throws AbstractException {
        @NotNull final ProjectUpdateResponse response = new ProjectUpdateResponse();
        @NotNull final ProjectDTO requestProject = request.getProject();
        @NotNull final ProjectDTO responseProject = service.update(UserUtil.getUserId(), requestProject);
        response.setProject(responseProject);
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload @NotNull final ProjectFindAllRequest request) throws AbstractException {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        @NotNull final List<ProjectDTO> responseProjects = service.findAll(UserUtil.getUserId());
        response.setProjects(responseProjects);
        return response;
    }


    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload @NotNull final ProjectClearRequest request) throws AbstractException {
        @NotNull final ProjectClearResponse response = new ProjectClearResponse();
        service.clear(UserUtil.getUserId());
        return response;
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload @NotNull final ProjectCountRequest request) throws AbstractException {
        @NotNull final ProjectCountResponse response = new ProjectCountResponse();
        final long responseCount = service.count(UserUtil.getUserId());
        response.setCount(responseCount);
        return response;
    }

}

