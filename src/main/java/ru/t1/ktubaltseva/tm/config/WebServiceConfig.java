package ru.t1.ktubaltseva.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.ktubaltseva.tm.endpoint.soap.AuthSoapEndpoint;
import ru.t1.ktubaltseva.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.t1.ktubaltseva.tm.endpoint.soap.TaskSoapEndpoint;
import ru.t1.ktubaltseva.tm.endpoint.soap.UserSoapEndpoint;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/ws/*");
    }

    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdl11Definition(@NotNull final XsdSchema projectEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdl11Definition(@NotNull final XsdSchema taskEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @NotNull
    @Bean(name = "AuthEndpoint")
    public DefaultWsdl11Definition authWsdl11Definition(@NotNull final XsdSchema authEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(AuthSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(AuthSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(AuthSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(authEndpointSchema);
        return wsdl11Definition;
    }

    @NotNull
    @Bean(name = "UserEndpoint")
    public DefaultWsdl11Definition userWsdl11Definition(@NotNull final XsdSchema userEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(UserSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(UserSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(UserSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(userEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

    @Bean
    @NotNull
    public XsdSchema authEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/authEndpoint.xsd"));
    }

    @Bean
    @NotNull
    public XsdSchema userEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/userEndpoint.xsd"));
    }

}
