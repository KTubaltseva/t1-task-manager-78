package ru.t1.ktubaltseva.tm.controller.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.CustomUser;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;

@Controller
public class TaskController {

    @Autowired
    private ITaskDTOService taskService;

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("/task/create")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public String create(
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) throws EntityNotFoundException {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(user.getUserId());
        taskService.add(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public String delete(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") final String id
    ) throws AbstractException {
        taskService.deleteById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") final String id
    ) throws AbstractException {
        @NotNull final TaskDTO task = taskService.findById(user.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @PostMapping("/task/edit/{id}")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public String edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @ModelAttribute("task") TaskDTO task,
            BindingResult result
    ) throws EntityNotFoundException {
        task.setUserId(user.getUserId());
        taskService.add(task);
        return "redirect:/tasks";
    }

}
