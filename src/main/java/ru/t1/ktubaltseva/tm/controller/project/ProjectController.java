package ru.t1.ktubaltseva.tm.controller.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.CustomUser;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;

@Controller
public class ProjectController {

    @Autowired
    private IProjectDTOService projectService;

    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @GetMapping("/project/create")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public String create(
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) throws EntityNotFoundException {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getUserId());
        projectService.add(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public String delete(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") final String id
    ) throws AbstractException {
        projectTaskService.deleteByUserIdAndProjectId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") final String id
    ) throws AbstractException {
        @NotNull final ProjectDTO project = projectService.findById(user.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @PostMapping("/project/edit/{id}")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public String edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @ModelAttribute("project") ProjectDTO project,
            BindingResult result
    ) throws EntityNotFoundException {
        project.setUserId(user.getUserId());
        projectService.add(project);
        return "redirect:/projects";
    }

}
