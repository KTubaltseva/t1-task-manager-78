package ru.t1.ktubaltseva.tm.controller.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.CustomUser;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Controller
public class TasksController {

    @Autowired
    private ITaskDTOService taskService;

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("/tasks")
    @Secured({"ROLE_USUAL", "ROLE_ADMIN"})
    public ModelAndView index(@AuthenticationPrincipal @NotNull final CustomUser user) throws AbstractException {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        modelAndView.addObject("tasks", taskService.findAll(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}

