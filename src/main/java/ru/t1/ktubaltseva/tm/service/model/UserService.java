package ru.t1.ktubaltseva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IUserService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.UserRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private UserRepository repository;

    @NotNull
    @Transactional
    public User add(@Nullable final User model) throws EntityNotFoundException {
        if (model == null) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @NotNull
    @Transactional
    public Collection<User> create(@Nullable final Collection<User> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return repository.saveAll(models);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws IdEmptyException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    public List<User> findAll() {
        return repository.findAll();
    }

    @NotNull
    public User findById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<User> optionalM = repository.findById(id);
        return optionalM.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    @Transactional
    public void delete(@Nullable final User model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteById(id);
    }

    @NotNull
    @Transactional
    public Collection<User> set(@Nullable final Collection<User> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return create(models);
    }

    @NotNull
    @Transactional
    public User update(
            @Nullable final User model
    ) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @Transactional
    public @NotNull User create() throws EntityNotFoundException {
        return add(new User());
    }

    @Nullable
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login).orElse(null);
    }

    @Nullable
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email).orElse(null);
    }

    @NotNull
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.existsByLogin(login);
    }

    @NotNull
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.existsByEmail(email);
    }

    @NotNull
    @Transactional
    public User lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        if (resultUser == null) throw new UserNotFoundException();
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        repository.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        repository.deleteByEmail(email);
    }

    @NotNull
    @Transactional
    public User unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

}
