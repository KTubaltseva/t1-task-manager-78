package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.UserDTORepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserDTOService implements IUserDTOService {

    @NotNull
    @Autowired
    private UserDTORepository repository;

    @NotNull
    @Transactional
    public UserDTO add(@Nullable final UserDTO model) throws EntityNotFoundException {
        if (model == null) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @NotNull
    @Transactional
    public Collection<UserDTO> create(@Nullable final Collection<UserDTO> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return repository.saveAll(models);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws IdEmptyException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    public List<UserDTO> findAll() {
        return repository.findAll();
    }

    @NotNull
    public UserDTO findById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<UserDTO> optionalM = repository.findById(id);
        return optionalM.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    @Transactional
    public void delete(@Nullable final UserDTO model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteById(id);
    }

    @NotNull
    @Transactional
    public Collection<UserDTO> set(@Nullable final Collection<UserDTO> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return create(models);
    }

    @NotNull
    @Transactional
    public UserDTO update(
            @Nullable final UserDTO model
    ) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    public @NotNull UserDTO create() throws EntityNotFoundException {
        return add(new UserDTO());
    }

    @NotNull
    public UserDTO findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login).orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    public UserDTO findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email).orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.existsByLogin(login);
    }

    @NotNull
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.existsByEmail(email);
    }

    @NotNull
    public UserDTO lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        repository.deleteByLogin(login);
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        repository.deleteByEmail(email);
    }

    @NotNull
    public UserDTO unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

}
