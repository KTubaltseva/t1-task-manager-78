package ru.t1.ktubaltseva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.TaskRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Transactional
    public Task add(@Nullable final Task model) throws EntityNotFoundException {
        if (model == null) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @NotNull
    @Transactional
    public Collection<Task> create(@Nullable final Collection<Task> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return repository.saveAll(models);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws IdEmptyException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    public Task findById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<Task> optionalM = repository.findById(id);
        return optionalM.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    @Transactional
    public void delete(@Nullable final Task model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteById(id);
    }

    @NotNull
    @Transactional
    public Collection<Task> set(@Nullable final Collection<Task> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return create(models);
    }

    @NotNull
    @Transactional
    public Task update(
            @Nullable final Task model
    ) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        repository.deleteAllByUser(user);
    }

    @Override
    public boolean existsById(@Nullable final User user, @Nullable final String id) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserAndId(user, id);
    }

    @NotNull
    public List<Task> findAll(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return repository.findAllByUser(user);
    }

    @NotNull
    public Task findById(@Nullable final User user, @Nullable final String id) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<Task> optionalM = repository.findByUserAndId(user, id);
        return optionalM.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public long count(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return repository.countByUser(user);
    }

    @Override
    @Transactional
    public void delete(@Nullable final User user, @Nullable final Task model) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.deleteByUserAndId(user, model.getId());
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final User user, @Nullable final String id) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteByUserAndId(user, id);
    }

    @NotNull
    @Transactional
    public Task update(
            @Nullable final User user,
            @Nullable final Task model
    ) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!repository.existsByUserAndId(user, model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @Override
    @Transactional
    public @NotNull Task create() throws EntityNotFoundException {
        return add(new Task());
    }

    @Override
    public @NotNull List<Task> findAllByUserAndProject(@Nullable final User user, @Nullable final Project project) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (project == null) throw new ProjectNotFoundException();
        return repository.findAllByUserAndProject(user, project);
    }

    @Override
    @Transactional
    public @NotNull Task create(@Nullable final User user) throws EntityNotFoundException, UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final Task task = new Task();
        task.setUser(user);
        return add(task);
    }

    @NotNull
    @Transactional
    public Task add(@Nullable final User user, @Nullable final Task model) throws EntityNotFoundException, UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUser(user);
        return repository.saveAndFlush(model);
    }

}
