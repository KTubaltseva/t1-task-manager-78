package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractDTORepository<TaskDTO> {

    long countByUserId(@Nullable final String userId);

    void deleteAllByUserId(@Nullable final String userId);

    void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id);

    boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id);

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable final String userId);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@Nullable final String userId, @Nullable final String id);

    long countByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId);

    void deleteAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId);

}
