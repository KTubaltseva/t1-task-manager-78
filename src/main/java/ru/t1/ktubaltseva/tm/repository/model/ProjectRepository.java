package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractRepository<Project> {

    long countByUser(@Nullable final User user);

    void deleteAllByUser(@Nullable final User user);

    void deleteByUserAndId(@Nullable final User user, @Nullable final String id);

    boolean existsByUserAndId(@Nullable final User user, @Nullable final String id);

    @NotNull
    List<Project> findAllByUser(@Nullable final User user);

    @NotNull
    Optional<Project> findByUserAndId(@Nullable final User user, @Nullable final String id);

}
