package ru.t1.ktubaltseva.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class MessageDTO {

    @NotNull
    private String value;

    public MessageDTO(@NotNull final String value) {
        this.value = value;
    }

}

