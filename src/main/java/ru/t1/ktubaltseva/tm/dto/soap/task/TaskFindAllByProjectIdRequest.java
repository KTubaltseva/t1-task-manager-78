package ru.t1.ktubaltseva.tm.dto.soap.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindAllByProjectIdRequest")
public class TaskFindAllByProjectIdRequest {

    @XmlElement(required = true)
    protected String projectId;

    public TaskFindAllByProjectIdRequest(@NotNull final String projectId) {
        this.projectId = projectId;
    }

}
