package ru.t1.ktubaltseva.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@Setter
public class CustomUser extends User {

    @NotNull
    private String userId;

    public CustomUser(@NotNull final UserDetails user) {
        super(user.getUsername(), user.getPassword(), user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(), user.isAccountNonLocked(), user.getAuthorities());
    }

    public CustomUser withUserId(@NotNull final String userId) {
        this.userId = userId;
        return this;
    }

}
