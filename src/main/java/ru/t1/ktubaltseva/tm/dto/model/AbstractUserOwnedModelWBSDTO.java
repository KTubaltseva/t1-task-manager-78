package ru.t1.ktubaltseva.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModelWBSDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @XmlElement
    @Column(name = "name")
    protected String name = "";

    @Nullable
    @XmlElement
    @Column(name = "description")
    protected String description;

    @NotNull
    @XmlElement
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @XmlElement
    @Column(name = "dateStart")
    @XmlSchemaType(name = "dateTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    @Nullable
    @XmlElement
    @Column(name = "dateFinish")
    @XmlSchemaType(name = "dateTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    public AbstractUserOwnedModelWBSDTO(@NotNull final String name) {
        this.name = name;
    }

    public AbstractUserOwnedModelWBSDTO(
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public AbstractUserOwnedModelWBSDTO(
            @NotNull final UserDTO user,
            @NotNull final String name
    ) {
        this.userId = user.getId();
        this.name = name;
    }

    public AbstractUserOwnedModelWBSDTO(
            @NotNull final UserDTO user,
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.userId = user.getId();
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}
