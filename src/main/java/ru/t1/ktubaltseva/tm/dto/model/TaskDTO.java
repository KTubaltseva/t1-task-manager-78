package ru.t1.ktubaltseva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@XmlAccessorType(XmlAccessType.FIELD)
public final class TaskDTO extends AbstractUserOwnedModelWBSDTO {

    @Nullable
    @XmlElement
    @Column(name = "project_id")
    private String projectId;

    public TaskDTO(@Nullable String name) {
        super(name);
    }

}
