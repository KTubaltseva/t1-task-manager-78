package ru.t1.ktubaltseva.tm.dto.soap.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "userFindByIdRequest")
public class UserFindByIdRequest {

    @XmlElement(required = true)
    protected String id;

    public UserFindByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
