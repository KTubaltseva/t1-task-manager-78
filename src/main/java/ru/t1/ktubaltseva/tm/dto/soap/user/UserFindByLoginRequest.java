package ru.t1.ktubaltseva.tm.dto.soap.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "userFindByLoginRequest")
public class UserFindByLoginRequest {

    @XmlElement(required = true)
    protected String login;

    public UserFindByLoginRequest(@NotNull final String login) {
        this.login = login;
    }

}
