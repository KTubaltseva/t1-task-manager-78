package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;

import java.util.List;

public interface IProjectDTOService extends IDTOService<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable String userId) throws EntityNotFoundException, UserNotFoundException;

    @NotNull
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model) throws EntityNotFoundException, UserNotFoundException;

    void clear(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    ProjectDTO findById(@Nullable String userId, @Nullable String id) throws AbstractException;

    long count(@Nullable String userId) throws AbstractException;

    void delete(@Nullable String userId, @Nullable ProjectDTO model) throws AbstractException;

    void deleteById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    ProjectDTO update(@Nullable String userId, @Nullable ProjectDTO model) throws AbstractException;

}
