package ru.t1.ktubaltseva.tm.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.dto.soap.project.*;
import ru.t1.ktubaltseva.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface IProjectSoapEndpoint {

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectCountResponse count(
            @RequestPayload @NotNull final ProjectCountRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectClearResponse clear(
            @RequestPayload @NotNull final ProjectClearRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final ProjectDeleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectExistsByIdResponse existsById(
            @RequestPayload @NotNull final ProjectExistsByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectFindAllResponse findAll(
            @RequestPayload @NotNull final ProjectFindAllRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectFindByIdResponse findById(
            @RequestPayload @NotNull final ProjectFindByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectCreateResponse create(@RequestPayload @NotNull ProjectCreateRequest request) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectAddRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectAddResponse add(
            @RequestPayload @NotNull final ProjectAddRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectUpdateResponse update(
            @RequestPayload @NotNull final ProjectUpdateRequest request
    ) throws AbstractException;

}
