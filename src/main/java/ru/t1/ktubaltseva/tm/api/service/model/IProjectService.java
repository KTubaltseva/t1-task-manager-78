package ru.t1.ktubaltseva.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @NotNull
    Project create(@Nullable User user) throws EntityNotFoundException, UserNotFoundException;

    @NotNull
    Project add(@Nullable User user, @Nullable Project model) throws EntityNotFoundException, UserNotFoundException;

    void clear(@Nullable User user) throws AbstractException;

    boolean existsById(@Nullable User user, @Nullable String id) throws AbstractException;

    @NotNull
    List<Project> findAll(@Nullable User user) throws AbstractException;

    @NotNull
    Project findById(@Nullable User user, @Nullable String id) throws AbstractException;

    long count(@Nullable User user) throws AbstractException;

    void delete(@Nullable User user, @Nullable Project model) throws AbstractException;

    void deleteById(@Nullable User user, @Nullable String id) throws AbstractException;

    @NotNull
    Project update(@Nullable User user, @Nullable Project model) throws AbstractException;

}
