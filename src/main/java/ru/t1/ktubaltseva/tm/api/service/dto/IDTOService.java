package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.AbstractModelDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;

import java.util.Collection;
import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> {

    @NotNull
    M create() throws EntityNotFoundException;

    @NotNull
    M add(@Nullable M model) throws EntityNotFoundException;

    @NotNull
    Collection<M> create(@Nullable Collection<M> models);

    void clear();

    boolean existsById(@Nullable String id) throws IdEmptyException;

    @NotNull
    List<M> findAll();

    @NotNull
    M findById(@Nullable String id) throws IdEmptyException, EntityNotFoundException;

    long count();

    void delete(@Nullable M model) throws AbstractException;

    void deleteById(@Nullable String id) throws IdEmptyException, EntityNotFoundException;

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    @NotNull
    M update(@Nullable M model) throws AbstractException;

}
