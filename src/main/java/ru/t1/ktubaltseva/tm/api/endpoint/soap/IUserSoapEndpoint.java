package ru.t1.ktubaltseva.tm.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.dto.soap.user.*;
import ru.t1.ktubaltseva.tm.endpoint.soap.UserSoapEndpoint;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;

public interface IUserSoapEndpoint {

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userCountRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserCountResponse count(
            @RequestPayload @NotNull final UserCountRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userClearRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserClearResponse clear(
            @RequestPayload @NotNull final UserClearRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userDeleteByIdRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final UserDeleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userExistsByIdRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserExistsByIdResponse existsById(
            @RequestPayload @NotNull final UserExistsByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userFindAllRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserFindAllResponse findAll(
            @RequestPayload @NotNull final UserFindAllRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userFindByIdRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserFindByIdResponse findById(
            @RequestPayload @NotNull final UserFindByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userFindByLoginRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserFindByLoginResponse findByLogin(
            @RequestPayload @NotNull final UserFindByLoginRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userSaveRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserSaveResponse save(
            @RequestPayload @NotNull final UserSaveRequest request
    ) throws EntityNotFoundException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "userUpdateRequest", namespace = UserSoapEndpoint.NAMESPACE)
    UserUpdateResponse update(
            @RequestPayload @NotNull final UserUpdateRequest request
    ) throws AbstractException;

}
