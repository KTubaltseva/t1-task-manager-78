package ru.t1.ktubaltseva.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/users")
public interface IUserRestEndpoint {

    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    long count() throws AbstractException;

    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    void clear() throws AbstractException;

    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    List<UserDTO> findAll() throws AbstractException;

    @NotNull
    @GetMapping(value = "/find/{id}", produces = APPLICATION_JSON_VALUE)
    UserDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @PutMapping(value = "/add", produces = APPLICATION_JSON_VALUE)
    UserDTO add(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityNotFoundException;

    @NotNull
    @PostMapping(value = "/update", produces = APPLICATION_JSON_VALUE)
    UserDTO update(
            @RequestBody @NotNull final UserDTO user
    ) throws AbstractException;

}
