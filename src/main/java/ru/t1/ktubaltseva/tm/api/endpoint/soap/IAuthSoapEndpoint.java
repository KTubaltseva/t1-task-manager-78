package ru.t1.ktubaltseva.tm.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.dto.soap.auth.*;
import ru.t1.ktubaltseva.tm.endpoint.soap.AuthSoapEndpoint;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface IAuthSoapEndpoint {

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "loginRequest", namespace = AuthSoapEndpoint.NAMESPACE)
    AuthLoginResponse login(
            @RequestPayload @NotNull final AuthLoginRequest request
    );

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "logoutRequest", namespace = AuthSoapEndpoint.NAMESPACE)
    AuthLogoutResponse logout(
            @RequestPayload @NotNull final AuthLogoutRequest request
    );

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "profileRequest", namespace = AuthSoapEndpoint.NAMESPACE)
    AuthProfileResponse profile(
            @RequestPayload @NotNull final AuthProfileRequest request
    ) throws AbstractException;

}
