FROM openjdk:8
WORKDIR /opt
EXPOSE 8080
COPY target/task-manager.war .
ENTRYPOINT ["java", "-jar", "task-manager.war"]